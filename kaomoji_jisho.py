# 顔文字の辞書
# daedroy.1457@gmail.com twitter:@Daedroy 坂本　隆昌（たかあき）が書いた

mk_jisho = {"ほほ笑み":'（＾-＾）',"泣き":"（TーT）", "謝り":"m(_ _)m",
    "メモをとる":"((φ(．．。)カキカキ","てれる":"(〃'∇'〃)ゝ"
}

mk_narabi = (
  ("ほほ笑み",'（＾-＾）'),("泣き","（TーT）"), ("謝り","m(_ _)m"),
  ("メモをとる","((φ(．．。)カキカキ"),("てれる","(〃'∇'〃)ゝ"), ("あせる","∑(=ﾟωﾟ=;)")
)
