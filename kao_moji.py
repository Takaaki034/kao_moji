# 顔文字を扱うソフト
# daedroy.1457@gmail.com twitter:@Daedroy 坂本　隆昌（たかあき）が書いた

from tkinter import Tk
from kaomoji_jisho import mk_narabi
from clipboard import Clipboard
#mk_narabi = (("ほほ笑み",'（＾-＾）'),("泣き","（TーT）"), ("謝り","m(_ _)m"),
#             ("メモをとる","((φ(．．。)カキカキ"),("てれる","(〃'∇'〃)ゝ"))

if __name__ == '__main__':
    i = 1
    s = ""
    while s != '0':
        print("0:終わる", end = " ")
        for youso in mk_narabi:
            print(str(i) + ":" +youso[0], end = " ")
            i += 1

        s = input('\n番号を入れ置く（入力）：')
        if s == '0': break
        kao = mk_narabi[int(s) - 1][1]
        print(kao)
        with Clipboard() as clipboard:
            clipboard.empty()
            clipboard.set_unicode_text(kao)

